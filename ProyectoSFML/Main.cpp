#include <SFML/Window.hpp>
#include <iostream>
#include <SFML/Graphics.hpp>
#define         SPRITE_SPEED        1

int main()
{
	sf::RenderWindow window(sf::VideoMode(800, 600), "Oh Yeh");
	
	sf::Texture texture;
	sf::Sprite sprite;
	sf::Clock timer;

	window.setKeyRepeatEnabled(false);

	texture.loadFromFile("Mask.png");

	sprite.setTexture(texture);
	
	int x = window.getSize().x / 2.;
	int y = window.getSize().y / 2.;
	
	bool upFlag = false;
	bool downFlag = false;
	bool leftFlag = false;
	bool rightFlag = false;

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			if (event.type == sf::Event::KeyPressed)
			{
				switch (event.key.code)
				{
					
				case  sf::Keyboard::Escape: window.close(); break;

					
				case sf::Keyboard::Up:     upFlag = true; break;
				case sf::Keyboard::Down:    downFlag = true; break;
				case sf::Keyboard::Left:    leftFlag = true; break;
				case sf::Keyboard::Right:   rightFlag = true; break;
				default: break;
				}
			}
			if (event.type == sf::Event::KeyReleased)
			{
				switch (event.key.code)
				{
					
				case sf::Keyboard::Up:     upFlag = false; break;
				case sf::Keyboard::Down:    downFlag = false; break;
				case sf::Keyboard::Left:    leftFlag = false; break;
				case sf::Keyboard::Right:   rightFlag = false; break;
				default: break;
				}
			}
			
		}
		if (leftFlag) x -= SPRITE_SPEED;
		if (rightFlag) x += SPRITE_SPEED;
		if (upFlag) y -= SPRITE_SPEED;
		if (downFlag) y += SPRITE_SPEED;

		
		if (x<0) x = 0;
		if (x>(int)window.getSize().x) x = window.getSize().x;
		if (y<0) y = 0;
		if (y>(int)window.getSize().y) y = window.getSize().y;

		window.clear(sf::Color(127, 127, 127));

		
		sprite.setPosition(x, y);
		window.draw(sprite);
			
		window.display();
	}

	return 0;
}
